﻿var app = require('express').createServer();
var io = require('socket.io').listen(app);
app.listen(3456);
var serveStatic = require('serve-static'),
	fs = require('fs');
	
app.use(serveStatic("public_html", {
	"index": "index.html"
}));

// rooms which are currently available in chat
var rooms = {};

//create default room
var roomsModel = require("./room");
var roomObj1 = new roomsModel.room('Default1','System',false,'none');
rooms['Default1'] = roomObj1;


io.sockets.on('connection', function (socket) {

	//first connection
	socket.on('adduser', function(username){
		socket.username = username;
		socket.room = 'Default1';
		rooms['Default1'].users[username] = {
                    userName:username,
                    roomName:'Default1'
                };
		
		//user join default room
		socket.join('Default1');
		socket.emit('feedback',username);
		socket.emit('updatechat', 'System', 'you join '+'Default1');
		io.sockets.in('Default1').emit('updateroomuser', rooms['Default1'].users);
		socket.broadcast.to('Default1').emit('updatechat', 'System', username + ' join');
		socket.emit('updaterooms', rooms, 'Default1');
	});
	
	//create room
	socket.on('creatroom',function(rname,username,porp,pwd){
		var roomObjTemp=new roomsModel.room(rname,username,porp,pwd);
		rooms[rname]=roomObjTemp;
		for (var key in rooms) {
			io.sockets.in(key).emit('updaterooms', rooms, socket.room);
		}
	});


	//send message and refresh
	socket.on('sendchat', function (data,flag1) {
		if (flag1=='all') {
			io.sockets.in(socket.room).emit('updatechat', socket.username, data);
		}else{
			if (data=='left999') {
				rooms[socket.room].blacklist= new function () {
					this[flag1] = {
						u: flag1
					};
				};
			}
			io.sockets.in(socket.room).emit('updatechat1', socket.username, data,flag1);
			
		}
		
	});
	
	//choose room
	socket.on('switchRoom', function(newroom){
		var f=true;
		for (var key in rooms[newroom].blacklist){
			if (key==socket.username) {
				f=false;
			}
		}
		if (f) {
		//old room refresh
		socket.join(socket.room);
		delete rooms[socket.room].users[socket.username];
		socket.broadcast.to(socket.room).emit('updateroomuser', rooms[socket.room].users);
		socket.broadcast.to(socket.room).emit('updatechat', 'System: ', socket.username+' left');
		//leave old room
		socket.leave(socket.room);
		//join new room
		socket.room = newroom;
		socket.join(socket.room);
		socket.emit('updatechat', 'System', 'Join '+ newroom);
		socket.broadcast.to(newroom).emit('updatechat', 'System: ', socket.username+' Join');
		rooms[newroom].users[socket.username] = {
                    userName:socket.username,
                    roomName:newroom
                };
		io.sockets.in(newroom).emit('updateroomuser', rooms[newroom].users,rooms[newroom].creator);	
		io.sockets.in(newroom).emit('updaterooms', rooms, newroom);
		}else{
			socket.emit('al');
		}
	});

	//when disconnect
	socket.on('disconnect', function(){
		delete rooms[socket.room].users[socket.username];
		io.sockets.in(socket.room).emit('updateroomuser', rooms[socket.room].users);
		io.sockets.in(socket.room).emit('updatechat', 'System', socket.username + ' disconnect');
		socket.leave(socket.room);
	});
});